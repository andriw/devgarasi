<?php

namespace App\Http\Livewire\Auth;

use Livewire\Component;

class Login extends Component
{
    public $email = '';
    public $password = '';

    protected $rules = [
        'email' => 'required',
        'password' => 'required',
    ];

    public function login()
    {
        $credentials = $this->validate();

//        $fieldType = filter_var($this->email, FILTER_VALIDATE_EMAIL) ? 'email' : 'name';

        if(filter_var($this->email, FILTER_VALIDATE_EMAIL)){
            $fieldType = 'email';
        }else{
            $fieldType = is_numeric($this->email) ? 'phone' : 'name';
        }

        if (auth()->attempt(array($fieldType => strtolower($this->email), 'password' => $this->password))) {
            return redirect()->intended('/dashboard');
        } else {
            return $this->addError('email', trans('auth.failed'));
        }
    }

    public function render()
    {
        return view('livewire.auth.login')->layout('layouts.login-layout');
    }
}
