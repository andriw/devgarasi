<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="UTF-8">

    <title>{{ config('app.name', 'Garasi99 | Gerakan Berbagi Inspirasi') }}</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Baloo+Paaji+2&display=swap" rel="stylesheet">

    <link rel="icon" href="{{ URL::asset('images/garasi.png') }}" type="image/x-icon"/>
    <!-- Styles -->
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">

    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js" defer></script>

@livewireStyles

<!-- Scripts -->
    <script src="{{ mix('js/app.js') }}" defer></script>
</head>
<body class="antialiased font-sans bg-gray-200">
{{ $slot }}

@livewireScripts
</body>
</html>
